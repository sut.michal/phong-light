import numpy as np
from mytypes import *


class Light:
    def __init__(self, pos: Vector3D, directional: int, ambient: int):
        self.pos = pos
        self.directional = directional
        self.ambient = ambient

    def vector(self, point, normalized=True):
        v = np.array([self.pos[i] - point[i] for i in range(3)])
        if normalized:
            return v / np.linalg.norm(v)
        return v

class RGBLight:
    def __init__(self, pos: Vector3D, directional: RGB, ambient: RGB):
        self.pos = pos
        self.directional = np.array(directional)
        self.ambient = np.array(ambient)

    def vector(self, point, normalized=True):
        v = np.array([self.pos[i] - point[i] for i in range(3)])
        if normalized:
            return v / np.linalg.norm(v)
        return v


# light = RGBLight(pos=(10, 10, 10), directional=(255, 255, 255), ambient=(0, 255, 255))

