import numpy as np
from mytypes import *
from math import sqrt
from light import RGBLight
from camera import Camera
from utils import clamp_array, distance3d


class Sphere:
    def __init__(self, pos: Vector3D, radius: int, ka=0, kd=0.5, ks=0.5, n=50):
        self.pos = pos
        self.radius = radius
        self.ka = ka
        self.kd = kd
        self.ks = ks
        self.points = self.generate_points()
        self.points2d = self.get_2d_points()
        self.n = n
        self.rendered_points2d = []

    @property
    def r(self):
        return self.radius

    def normal(self, point, normalized = True):
        v = np.array([point[i] - self.pos[i] for i in range(3)])
        if normalized:
            return v / np.linalg.norm(v)
        return v

    def generate_points(self):
        if hasattr(self, 'points') and self.points:
            return self.points
        circle_points = []
        for _x in range(int(self.pos[0] - self.radius), int(self.pos[0] + self.radius + 1)):
            for _y in range(int(self.pos[1] - self.radius), int(self.pos[1] + self.radius + 1)):
                if self._distance(_x, _y) < self.radius:
                    z = self._get_z(_x, _y)
                    circle_points.append((_x, _y, z))
        return circle_points

    def _distance(self, _x, _y):
        return round(sqrt((self.pos[0] - _x)*(self.pos[0] - _x) + (self.pos[1] - _y)*(self.pos[1] - _y)))

    def _get_z(self, _x, _y):
        x0, y0, z0 = self.pos
        r = self.radius
        c = (_x - x0) ** 2 + (_y - y0) ** 2 - r ** 2

        z1 = z0 + sqrt(z0 ** 2 - c)
        z2 = z0 - sqrt(z0 ** 2 - c)
        return round(max(z1, z2))

    def get_2d_points(self):
        if hasattr(self, 'points2d') and self.points2d:
            return self.points2d
        return [(x, y) for x, y, z in self.points]

    def reflected(self, light, point, normalized=True):
        v = 2 * np.dot(light.vector(point), self.normal(point)) * self.normal(point) - light.vector(point)
        if normalized:
            return v / np.linalg.norm(v)
        return v

    def render(self, light: RGBLight, camera: Camera):
        self.rendered_points2d = []
        for point in self.points:
            Ia = self.ka * light.ambient
            Id = self.kd * max(0, np.dot(self.normal(point), light.vector(point))) * light.directional
            Is = self.ks * np.dot(self.reflected(light, point), camera.vector(point))**self.n * light.directional

            f = 1

            I = Ia + f*(Id + Is)
            I = clamp_array(I, 0, 255)
            self.rendered_points2d.append(
                ((point[0], point[1]), tuple(I))
            )
