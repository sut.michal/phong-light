from typing import Tuple, Union, List

# Types
RGB = Union[Tuple[int, int, int], List[int]]
Vector3D = Union[Tuple[float, float, float], List[float]]
