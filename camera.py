import numpy as np
from mytypes import *


class Camera:
    def __init__(self, pos: Vector3D):
        self.pos = pos

    @property
    def x(self):
        return self.pos[0]

    @property
    def y(self):
        return self.pos[1]

    @property
    def z(self):
        return self.pos[2]

    def vector(self, point, normalized = True):
        v = np.array([self.pos[i] - point[i] for i in range(3)])
        if normalized:
            return v / np.linalg.norm(v)
        return v
