import numpy as np
from math import sqrt


def clamp_array(array, _min, _max):
    new_array = []
    for item in array:
        new_array.append(max(_min, min(item, _max)))
    return np.array(new_array)


def distance3d(point_1, point_2):
    return sqrt((point_1[0] - point_2[0])**2 + (point_1[1] - point_2[1])**2 + (point_1[2] - point_2[2])**2)
