import pygame
import sys
from sphere import Sphere
from light import RGBLight
from camera import Camera
from mytypes import Vector3D

pygame.init()

WIDTH = 640
HEIGHT = 480
CENTER = (WIDTH//2, HEIGHT//2)
CENTER3D: Vector3D = (*CENTER, 0)

GREEN = (0, 255, 0)

screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("First Pygame Application")

clock = pygame.time.Clock()

light = RGBLight(pos=(200, 200, 300), directional=(255, 255, 255), ambient=(0, 255, 0))
camera = Camera(pos=(300, 200, 100))
sphere = Sphere(pos=CENTER3D, radius=200, ka=0.3, kd=0.4, ks=1, n=60)
sphere.render(light, camera)
print("rendered")

while True:
    clock.tick(50)

    # Process events
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()

    screen.fill((0, 0, 0))
    for point, color in sphere.rendered_points2d:
        # print(color)
        pygame.draw.line(screen, color, point, point)
    # Update the screen
    pygame.display.flip()
