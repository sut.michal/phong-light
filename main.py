import pygame
import sys
from math import sqrt
import numpy as np

pygame.init()

WIDTH = 640
HEIGHT = 480
CENTER = (WIDTH//2, HEIGHT//2)

GREEN = (0, 255, 0)

screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("First Pygame Application")

clock = pygame.time.Clock()


class Camera:
    def __init__(self, pos):
        self.pos = pos


class Light:
    def __init__(self, i, pos):
        self.i = i
        self.pos = pos


class Circle:
    def __init__(self, center, radius):
        self.center = (*center, 0)
        self.radius = radius
        self.points = None
        d = self.radius * 2
        self.points = self.generate_points()

    def generate_points(self):
        if self.points:
            return self.points
        circle_points = []
        for _x in range(self.center[0] - self.radius, self.center[0] + self.radius + 1):
            for _y in range(self.center[1] - self.radius, self.center[1] + self.radius + 1):
                if self._distance(_x, _y) < self.radius:
                    z = self._get_z(_x, _y)
                    circle_points.append((_x, _y, z))
        return circle_points

    def _distance(self, _x, _y):
        return round(sqrt((self.center[0] - _x)*(self.center[0] - _x) + (self.center[1] - _y)*(self.center[1] - _y)))

    def _get_z(self, _x, _y):
        x0, y0, z0 = self.center
        r = self.radius
        c = (_x - x0) ** 2 + (_y - y0) ** 2 - r ** 2

        z1 = z0 + sqrt(z0 ** 2 - c)
        z2 = z0 - sqrt(z0 ** 2 - c)
        return round(max(z1, z2))


def normalized(v):
    norm=np.linalg.norm(v)
    if norm==0:
        norm=np.finfo(v.dtype).eps
    return v/norm


def length(v):
    return sqrt(v[0] ** 2 + v[1] ** 2 + v[2] ** 2)


def get_intensity(_light, _sphere, _camera, _x, _y, _z):
    ka = 0
    kd = 0.5
    ks = 0.5
    f = 1
    ia = 255
    ii = _light.i
    n = 50

    N = np.array([
        _x - _sphere.center[0],
        _y - _sphere.center[1],
        _z - _sphere.center[2]
    ])

    N = normalized(N)

    L = normalized(np.array([
        _light.pos[0] - _x,
        _light.pos[1] - _y,
        _light.pos[2] - _z,
    ]))

    R = normalized(2*(np.dot(L, N)) * N - L)

    V = normalized(np.array([
        camera.pos[0] - _x,
        camera.pos[1] - _y,
        camera.pos[2] - _z,
    ]))

    # return ka * ia + ii * (kd * np.dot(L, N) + ks * f * np.dot(R, V)**n)
    return ka * ia + kd*ii*np.dot(N, L) + ks*ii*np.dot(V, R)**n


def get_color(i, color, point):

    i *= 200
    ff = 255*i
    # r, g, b = color[0], color[1] + ff, color[2]
    r, g, b = ff, color[1] + ff, ff
    r = max(0, min(r, 255))
    g = max(0, min(g, 255))
    b = max(0, min(b, 255))
    # z = ((point[2]) + 200 / 400)
    # r, g, b = color[0] * z, color[1] * z, color[2] * z
    return r, g, b


camera = Camera((*CENTER, 150))
light = Light(5, (CENTER[0]-100, CENTER[1], 100))
circle = Circle(CENTER, 200)
intensity_vector = []
for x, y, z in circle.points:
    intensity_vector.append(get_intensity(light, circle, camera, x, y, z))
intensity_vector = normalized(np.array(intensity_vector))
colors = []
for (x, y, z), i in zip(circle.points, intensity_vector):
    colors.append(get_color(i, GREEN, (x, y, z)))

while True:
    clock.tick(50)

    # Process events
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()

    screen.fill((0, 0, 0))

    for i, (x, y, z) in enumerate(circle.points):
        point = (x, y)
        pygame.draw.line(screen, colors[i], point, point)

    # Update the screen
    pygame.display.flip()
